function change(){
	var elemen = document.getElementById('theme')
	if (elemen.className == 'first'){
		document.getElementById('theme').className = 'firstbw';
		document.getElementById('button').className = 'btn btn-dark';
	}
	else{
		document.getElementById('theme').className = 'first';
		document.getElementById('button').className = 'btn btn-danger orange';
	}
}

function check_element(ele)
{
  var all = document.getElementsByTagName("*");
  var totalele=all.length;
  var per_inc=100/all.length;

  if($(ele).on())
  {
    var prog_width=per_inc+Number(document.getElementById("progress_width").value);
    document.getElementById("progress_width").value=prog_width;
    $("#bar1").animate({width:prog_width+"%"},10,function(){
      if(document.getElementById("bar1").style.width=="100%")
      {
        $(".progress").fadeOut("slow");
      }			
    });
  }

  else	
  {
    set_ele(ele);
  }
}

function set_ele(set_element)
{
  check_element(set_element);
}