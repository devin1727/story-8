from django.urls import re_path
from .views import index
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    re_path(r'^$', index, name="index"),
    ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)