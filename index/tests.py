from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from django.apps import apps
from .apps import IndexConfig

class Lab7Test(TestCase):
    # checking if the response succeded
    def test_lab7_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    # checking if there is a homepage function in views
    def test_lab7_homepage_function(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    # checking if the client got the html
    def test_lab7_html(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')

    def test_apps(self):
        self.assertEqual(IndexConfig.name, 'index')
        self.assertEqual(apps.get_app_config('index').name, 'index')